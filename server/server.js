var express = require('express'),
	app = express(),
	port = process.env.SERVER_PORT || 3000,
	mongoose = require('mongoose'),
	Task = require('./api/models/notesModel'), //created model loading here
	bodyParser = require('body-parser');

// mongoose instance connection url connection
mongoose.Promise = global.Promise;
var options = {
	useMongoClient: true,
	user: process.env.DB_USER,
	pass: process.env.DB_PASSWORD
}
mongoose.connect('mongodb://' + process.env.DB_HOST + ':' + process.env.DB_PORT + '/geekycamp', options);

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());


var routes = require('./api/routes/notesRoutes'); //importing route
routes(app); //register the route


app.listen(port);


console.log('RESTful API server started on: ' + port);