'use strict';
module.exports = function (app) {
	var notes = require('../controllers/notesController');

	// Notes Routes
	app.get('/notes', notes.list_all_notes);
	app.post('/notes', notes.create_a_note);
	app.delete('/notes/:noteId', notes.delete_a_note);
};