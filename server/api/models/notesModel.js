'use strict';
var mongoose = require('mongoose');
var Schema = mongoose.Schema;


var NotesSchema = new Schema({
  title: {
    type: String,
    required: 'Kindly enter the title of the note'
  },
  content: {
    type: String,
    required: 'Kindly enter the content of the note'
  }
});

module.exports = mongoose.model('Notes', NotesSchema);