## REQUIREMENTS
NodeJS
MongoDB
Web server - (Apache/Nginx/any other capable of running html)

# Step 1 - setting up the enviromental variables
PowerShell
$env:SERVER_PORT="";$env:DB_HOST="";$env:DB_PORT="";$env:DB_USER="";$env:DB_PASSWORD="";

CMD
set SERVER_PORT="";set DB_HOST="";set DB_PORT="";set DB_USER="";set DB_PASSWORD="";

## Step 2 - entering the server's dir
Terminal/PowerShell/CMD
cd <current-dir>/server

## Step 3 - starting the server
npm start

## Step 4 - set up the url and the port of your server
Open index.html change the string on line {X} to your server's address including its port and add "/notes" to its end
Example:
var url = 'http://localhost:3000/notes';

## Step 5 - move the contents of <current-dir>/web
## Step 6 - open and use your message board